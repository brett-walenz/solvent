import pandas
import time
import datetime
import argparse
from check.context import cacheable, computes, inputs
import check.permute.interval as interval
import check.utils as utils
import dukembb
from sqlalchemy import create_engine
import check.query as query

def sync(cache_queryable, results, columns):
    return cache_queryable

#our data looks like:
#(games, points)
#we want to find out how many streaks dominate a specific streak

@inputs(['games', 'points'])
#@computes(['t_games', 't_points'])
@computes(['t_games', 't_points'])
def transform_d(queryable, parameters, memory):
    return parameters
   
@computes(['dominated_by'], index='playerid')
def compute_d(queryable, parameters, cache):
    games, points = parameters
    #sql_query = ('SELECT games, points FROM data WHERE playerid = ?'
    #         ' AND g_start <= ? AND g_end >= ? AND points >= ?'
    #         ' AND (g_start < ? OR g_end > ?) ORDER BY g_end - g_start DESC')
    #results = queryable.sql(sql_query, (playerid, g_start, g_end, points, g_start, g_end))
    #if results:
    #    return (False, (-1,))
    columns = ['games', 'points']
    comparators = [query.GEQ, query.GEQ]

    dom_query = query.create_dominance_count_query(columns, comparators, (games, points))
    dominated = queryable.sql(dom_query).fetchone()
    rval = True
    if dominated[0] > 25:
        rval = False
    results = [dominated[0]]
    return (rval, results)

def prune_d(cache, parameter_setting):
    return 2

pg_alc = "postgresql+psycopg2://fact_user:lzc1123@fact-m/factdb2"
engine = create_engine(pg_alc)
frame = pandas.read_sql("SELECT games, points FROM mbbstreaks", engine)
print("Number of entries: ", len(frame))
conn = engine.connect()
parameters = conn.execute('SELECT games, points FROM mbbstreaks').fetchall()

parser = argparse.ArgumentParser()
args = parser.parse_args()

t0 = time.time()

hostname = "fact-m"
taskname = "streak_dual_points"
task,log = dukembb.create_task(hostname, taskname, frame, parameters, conf='/home/vagrant/phd/dukembb/streaks/dual/pts/solvent-final.conf')
results = task.compute(transform_d, compute_d, prune_d, sync_function = sync)
task.post_process(transform_d, compute_d)
t1 = time.time()
print(t1-t0)
    



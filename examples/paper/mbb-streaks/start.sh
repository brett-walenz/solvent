#!/bin/bash
(cd /home/vagrant/phd/research/rip/research/unified ; python setup.py bdist_egg)
#psql -af drop.sql factdb2
#psql -ac "DROP TABLE mbbstreaks;" factdb2
redis-cli flushall
/home/hadoop/spark-install/bin/spark-submit --master spark://fact-m:7070 --py-files /home/vagrant/phd/research/rip/research/unified/dist/check-0.0.1-py2.7.egg approx_streak.py > approx_streak.log

redis-cli flushall
/home/hadoop/spark-install/bin/spark-submit --master spark://fact-m:7070 --py-files /home/vagrant/phd/research/rip/research/unified/dist/check-0.0.1-py2.7.egg streak_dom.py > streak_dom.log


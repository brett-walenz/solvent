import pandas
import time
import datetime
import argparse
from check.context import cacheable, computes, inputs, generative
import check.permute.interval as interval
import check.utils as utils
import dukembb
from sqlalchemy import create_engine
import check.query as query
from check.sync import add_all
from operator import itemgetter

DISTINCT_GAMES_QUERY = ("(SELECT DISTINCT A.gameid AS gameid "
                      "FROM data AS A, data AS B "
                      "WHERE A.season = B.season "
                      "AND A.gameid = B.gameid AND "
                      "A.playerid = :left_playerid AND B.playerid = :right_playerid) ")
SQLITE_GAMES_QUERY = ("SELECT L.gameid AS start, R.gameid AS end "
                     "FROM " + DISTINCT_GAMES_QUERY + " AS L, " + DISTINCT_GAMES_QUERY
                     + " AS R WHERE L.gameid < R.gameid")

print(SQLITE_GAMES_QUERY)

SQLITE_POINTS_QUERY_2 = ("SELECT min(tp) as points, count(1) as games FROM data WHERE playerid = :playerid "
        "AND gameid >= :start AND gameid <= :end")

SQLITE_POINTS_QUERY = ("SELECT min(tp) as points, count(1) as games FROM data WHERE (playerid = ? OR playerid = ?) AND gameid >= ? AND gameid <= ?")

@cacheable
@generative()
@inputs(['left_playerid', 'right_playerid'])
@computes(['games', 'points'], index='playerid')
def transform_d(data, parameter_setting, memory):
    left_playerid, right_playerid = parameter_setting
    streaks = data.sql(SQLITE_GAMES_QUERY, left_playerid=left_playerid, right_playerid=right_playerid).fetchall()

    streaks.sort(key=itemgetter(0))    
    for i, (start, end) in enumerate(streaks):
        l_points, l_games = data.sql(SQLITE_POINTS_QUERY_2, playerid=left_playerid, start=start, end=end).fetchone()
        r_points, r_games = data.sql(SQLITE_POINTS_QUERY_2, playerid=right_playerid, start=start, end=end).fetchone()
        if l_games != r_games or (not l_points or not r_points):
            continue
        
        min_pts = l_points
        if r_points < l_points:
            min_pts = r_points
            
        if min_pts == 0 or min_pts < 10 or l_games < 5:
            continue
        include = True
        for j, (o_start, o_end) in enumerate(streaks):
            if o_start > start:
                break
            ol_points, ol_games = data.sql(SQLITE_POINTS_QUERY_2, playerid=left_playerid, start=o_start, end=o_end).fetchone()
            or_points, or_games = data.sql(SQLITE_POINTS_QUERY_2, playerid=right_playerid, start=o_start, end=o_end).fetchone()
            min_inner_pts = ol_points
            if or_points < ol_points:
                min_inner_pts = or_points
            if i == j:
                continue
            if o_start <= start and o_end >= end and min_inner_pts >= min_pts and ol_games >= l_games:
                include = False
        if include:
            yield (l_games, min_pts)

def prune_d(cache, parameters):
    games, points = parameters
    sql_query = ('SELECT * FROM data WHERE games >= :games AND points >= :points AND '
            'dominated_by > :dominated_by LIMIT 1')
    results = cache.sql(sql_query, games=games, points=points, dominated_by=25).fetchone()
    if results and len(results) > 0:
        return 0
    return 2


@computes(['dominated_by'], index='playerid')
def compute_d(data, parameter_setting, cache):
    games, points = parameter_setting
    #execute the dominance query here
    columns = ['games', 'points']
    comparators = [query.GEQ, query.GEQ]

    dom_query = query.create_dominance_count_query(columns, comparators, (games, points))
    if not cache.is_blank():
        dominated = cache.sql(dom_query).fetchone()
        results = (dominated[0], )
    else:
        results = 0
    return (True, results)

pg_alc = "postgresql+psycopg2://fact_user:lzc1123@fact-m/factdb2"
engine = create_engine(pg_alc)
frame = pandas.read_sql("SELECT playerid, games.gameid, tp, games.season FROM mbb, games WHERE mbb.gameid = games.gameid", engine)
print(frame)
print("Number of entries: ", len(frame))
D_QUERY = ("SELECT DISTINCT A.playerid AS left_playerid, B.playerid AS "
           "right_playerid FROM mbb A, mbb B WHERE "
           "A.playerid < B.playerid AND A.gameid = B.gameid")
parameters = engine.execute(D_QUERY).fetchall()

parser = argparse.ArgumentParser()
args = parser.parse_args()

t0 = time.time()

hostname = "fact-m"
taskname = "dual_prominent_streak_prune"
task, log = dukembb.create_task(hostname, taskname, frame, parameters, conf='solvent-sql.conf')
results = task.compute(transform_d, compute_d, prune_d, sync_function = add_all)
task.post_process(transform_d, compute_d)

t1 = time.time()
print(t1-t0)
log.dump_detailed_stats()
log.dump_aggregate_stats()
                            

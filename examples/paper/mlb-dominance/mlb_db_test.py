import pandas as pd
import random
from sqlalchemy import create_engine
from sqlalchemy import text
import time
import sqlite3

columns = ['playerid', 'first_name', 'last_name', 'weight', 'height', 'year', 'round', 'team_id', 'league_id', 'games_pitched', 'games_pitched_started', 'p_shutouts', 'p_hits', 'p_strikeouts', 'p_walks', 'p_saves', 'p_earned_run_average', 'b_at_bats', 'b_runs', 'b_hits', 'b_doubles', 'b_triles', 'b_homeruns', 'b_runs_batted_in', 'b_stolen_bases', 'b_strikeouts', 'b_walks']

dblp_file = "/home/vagrant/phd/research/rip/research/unified/examples/gen/generated_mlb.csv"
df = pd.read_csv(dblp_file, header=None, index_col=False, names=columns)
df = df[['playerid', 'round', 'year', 'p_strikeouts', 'p_earned_run_average', 'p_hits']]

#engine = create_engine('postgresql://fact_user:lzc1123@localhost:5432/factdb')
engine = create_engine('sqlite://')
df.to_sql('data', engine)

#conn = sqlite3.connect(':memory:')
#df.to_sql('data', conn)
#c = conn.cursor()

average = 0 
start = time.time()
query = text('select playerid, year, round FROM data')
results = engine.execute(query)
engine.execute("CREATE INDEX idx1_pyr on data(playerid, year, round)")
results = results.fetchall()
random.shuffle(results)
end_index = len(results) / 16
player_set = results[0:end_index]
print("player set size:", len(player_set))
dominance = text('SELECT count(1) as dominance FROM data L'
            ', (SELECT p_strikeouts, p_earned_run_average, p_hits FROM data '
            'WHERE playerid = :playerid AND year = :year AND round = :rou) R'
            ' WHERE '
            'L.p_strikeouts >= R.p_strikeouts AND '
            'L.p_earned_run_average <= R.p_earned_run_average AND '
            'L.p_hits <= R.p_hits '
            ' AND '
            '(L.p_strikeouts > R.p_strikeouts OR '
            'L.p_earned_run_average < R.p_earned_run_average '
            'OR L.p_hits < R.p_hits)')
curr_cnt = 0
average += time.time() - start
for player_id, year, rou in player_set:
    per_player = time.time()
    results = engine.execute(dominance, playerid=player_id, year=year, rou=rou).fetchall()
    pp_end = time.time()
    average += pp_end - per_player
    curr_cnt += 1
    if curr_cnt % 50 == 0:
        print("average: ", average / float(curr_cnt))

end = time.time()
print("Total run time: ", end-start)

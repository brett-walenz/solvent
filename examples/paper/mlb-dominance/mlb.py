#import fcheck.config as config
import check.permute.interval as interval
import check.permute.combinations as combinations
import datetime
import StringIO
import csv
import numpy
from pyspark import SparkContext
import pandas as pd
import time
from check.executor import DistributedExecutor
from check.context import cacheable, computes, inputs
from check.context import GenericCheckComputeContext
from check.task import FixedMultiEpochTask 
#from check.task import SingleEpochTask
from check.sync import sync_dominance_cache
import argparse
import check.utils as utils
from functools import partial

columns = ['playerid', 'first_name', 'last_name', 'weight', 'height', 'year', 'round', 'team_id', 'league_id', 'games_pitched', 'games_pitched_started', 'p_shutouts', 'p_hits', 'p_strikeouts', 'p_walks', 'p_saves', 'p_earned_run_average', 'b_at_bats', 'b_runs', 'b_hits', 'b_doubles', 'b_triles', 'b_homeruns', 'b_runs_batted_in', 'b_stolen_bases', 'b_strikeouts', 'b_walks']

dblp_file = "/home/vagrant/phd/research/rip/research/unified/examples/gen/generated_mlb.csv"
df = pd.read_csv(dblp_file, header=None, index_col=False, names=columns)
df = df[['playerid', 'round', 'year', 'p_strikeouts', 'p_earned_run_average', 'p_hits']]
#parameters are really the playerid, round, year 
DOMINANCE = 50

dominance = ('FROM data WHERE (p_strikeouts >= :strikeouts AND p_earned_run_average <= :era AND p_hits < :hits) OR '
        '(p_strikeouts >= :strikeouts AND p_earned_run_average < :era AND p_hits <= :hits) OR '
        '(p_strikeouts > :strikeouts AND p_earned_run_average <= :era AND p_hits <= :hits)')

dom_sql = ('SELECT count(1) as dominance_count ') + dominance  

@cacheable
@inputs(['playerid', 'year', 'round'])
@computes(["p_strikeouts", "p_earned_run_average", "p_hits"])
def computation_parameters(data, parameters, memory):
    aid, year, rnd = parameters
    comparator = data.sql("SELECT p_strikeouts, p_earned_run_average, p_hits FROM data where playerid = :playerid AND year = :year AND round = :rnd", playerid=aid, year=year, rnd=rnd).fetchone()
    return comparator

@computes(["dominance_count"])
def calculate_dominance(data, computation_parameters, cache):
    strikeouts, era, hits = computation_parameters
    count = data.sql(dom_sql, strikeouts=strikeouts, era=era, hits=hits).fetchone()[0]
    response = count <= DOMINANCE
    return (response, count)

def prune_by_dominance(data, computation_parameters):
    #sql = ('PRAGMA table_info(data)')
    #result = data.conn.execute(sql)
    strikeouts, era, hits = computation_parameters 
    sql = ('SELECT count(1) FROM data WHERE p_strikeouts <= :strikeouts AND p_earned_run_average >= :era and p_hits >= :hits AND dominance_count <= :dominance_count')
    count = data.sql(sql, strikeouts=strikeouts, era=era, hits=hits, dominance_count=DOMINANCE).fetchone()[0] 
    print("Parameters: s:{0}, e:{1}, h:{2}, Count: {3}".format(strikeouts, era, hits, count))
    #if len(responses) > 0:
        #print("updated =================== params: ", params)
        #print("count: ", count)
        #for resp in responses:
            #print(resp)
    if count > 0:
        #these are the automatic 'YES' entries
        return True

    sql = ('SELECT count(1) FROM data WHERE p_strikeouts >= :strikeouts AND p_earned_run_average <= :era and p_hits <= :hits AND dominance_count >= :dominance_count')
    count = data.sql(sql, strikeouts=strikeouts, era=era, hits=hits, dominance_count=DOMINANCE).fetchone()[0]
    if count > 0:
        return False
    return 2

t0 = time.time()
params = df[['playerid', 'year', 'round']].values.tolist()
print("number of parameters: ", len(params))
sparkContext = SparkContext("spark://fact-m:7077", "FactCheck")

parser = argparse.ArgumentParser()
parser.add_argument('--epochs', nargs='*', type=float)
parser.add_argument('--seed', type=float)
parser.add_argument('--mini_batch_size', type=int, default=1000)
args = parser.parse_args()
print("ARGUMENTS")
print(args.seed)
print(args.epochs)
print(args.mini_batch_size)
log = utils.StatsLog("Seed Size({0}), Second Epoch({1})".format(args.seed, str(args.epochs)))
executor = DistributedExecutor(sparkContext, "fact-m", log)
conf = 'solvent-null.conf'
context = GenericCheckComputeContext(executor, log=log, conf=conf)
sync_function = partial(sync_dominance_cache, dominance=DOMINANCE)
task = FixedMultiEpochTask(context, df, params, epoch1=args.seed, epoch2=args.epochs, log=log)
#task = SingleEpochTask(context, df, params, log=log)
results = task.compute(computation_parameters, calculate_dominance, prune_by_dominance, sync_function=sync_function, mini_batch_size=args.mini_batch_size)

t1 = time.time()
print(t1-t0)
log.dump_detailed_stats()
log.dump_aggregate_stats()

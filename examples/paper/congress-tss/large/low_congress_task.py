import check
import pandas

from pyspark import SparkContext
import check.permute.interval as interval
import time
import datetime
import argparse
import check.utils as utils
from check.executor import DistributedExecutor
from check.context import cacheable, computes, generative, inputs
from check.context import GenericCheckComputeContext
from check.task import FixedMultiEpochTask 
from operator import itemgetter
import sqlite3
import random

columns = ['vote_id', 'person_id', 'vote', 'category', 'chamber', 'session', 'date', 'number', 'subject', 'results', 'first_name', 'last_name', 'birthday', 'id_govtrack']

home_dir = '/home/vagrant/phd/politics/'
filename = '2012-curr-full-votes.csv'
dataframe = pandas.read_csv(home_dir + filename, parse_dates=[6], header=None, index_col=False, names=columns)

dataframe = dataframe[['vote_id', 'person_id', 'vote', 'date', 'chamber']]

def sync(cache_queryable, results, columns):
    values_string = ""
    for c in columns: 
        values_string += "?, "

    curs = cache_queryable.conn.cursor()
    curs.executemany("INSERT INTO data VALUES (" + values_string[:-2] + ")", results)
    results = curs.execute("SELECT * FROM data WHERE (agree + disagree) <= 50")
    full_results = results.fetchall()
    curs.execute("DELETE FROM data")
    curs.executemany("INSERT INTO data VALUES (" + values_string[:-2] + ")", full_results)
    
    return cache_queryable

@computes(["percent", "agree", "disagree"], index='person_id')
def calculate_voting(queryable, (left_person, right_person, start, end), cache):
    query = ('SELECT left_vote, right_vote, left_date '
            'FROM (SELECT vote_id as left_vote_id, vote as left_vote, date as left_date '
            'FROM data where person_id = ?) LEFT_SIDE JOIN '
            '(SELECT vote_id as right_vote_id, vote as right_vote, date as right_date '
            'FROM data where person_id = ?) RIGHT_SIDE '
            'ON LEFT_SIDE.left_vote_id = RIGHT_SIDE.right_vote_id WHERE left_date >= DATE(?) AND left_date <= DATE(?)')
    results = queryable.sql(query, [left_person, right_person, start, end])
    agree = 0
    disagree = 0
    for left_vote, right_vote, left_date in results:
        if left_vote == right_vote:
            agree += 1
        else:
            disagree += 1
    percent = 0
    response = False
    if (agree + disagree) != 0:
        percent = agree / float((agree + disagree))
    if (agree + disagree) > 50:
        response = True 
    return (response, (percent, agree, disagree))
       
def prune_by_count(cache, (left_person, right_person, start, end)):
    #query = ('SELECT agree, disagree, start, end FROM data WHERE left_person = ? and right_person = ?')
    #results = cache.sql(query, [left_person, right_person])
    #for r in results:
    #    print(left_person, " ", right_person, " ", start, " ", end, " ", r)
    query = ('SELECT agree, disagree, start, end FROM data WHERE left_person = ? and right_person = ?'
            ' AND start <= DATE(?) AND end >= DATE(?)')
    results = cache.sql(query, [left_person, right_person, start, end])
    #print("Now checking refined query...", len(results))
    #for r in results:
    #    print(left_person, " ", right_person, " ", start, " ", end, " ", r)
    #if len(results) > 0:
    #    return 0 
    #prob = random.randint(1, 100)
    #if prob <= 99:
    #    return 0

    return 2

@generative()
@inputs(["left_person", "right_person", "generated_params"])
@computes(["left_person_id", "right_person_id", "start_date", "end_date"], index='person_id')
def transform_parameters(queryable, (left_person, right_person, generated_params), memory):
    if not 'generated_params' in memory:    
        start_range, end_range, min_width, max_width, freq = generated_params
        intervals = interval.interval_set(start_range, end_range, freq=freq, max_delta=max_width, min_delta=min_width)
        new_intervals = []
        for id, start, end in intervals:
            new_intervals.append((start, end, end-start))
        intervals = sorted(new_intervals, key=itemgetter(2), reverse=False)
        memory['generated_params'] = intervals
    else:
        intervals = memory['generated_params']
    query = "SELECT min(date) as min_date, max(date) as max_date FROM data where (person_id = ? OR person_id = ?) AND date >= DATE(?) AND date <= DATE(?)" 
    for start, end, _ in intervals:
         results = queryable.sql(query, [left_person, right_person, start, end])[0]
         yield (left_person, right_person, results[0], results[1])
        #yield (left_person, right_person, start, end)

sparkContext = SparkContext("spark://fact-m:7077", "All Vote Correlation")
parser = argparse.ArgumentParser()
parser.add_argument('--min_width', type=int, default=30)
parser.add_argument('--max_width', type=int, default=365)
parser.add_argument('--freq', type=str, default='15D')
parser.add_argument('--min_votes', type=int, default=50)
args = parser.parse_args()

min_width = pandas.Timedelta(days=args.min_width)
max_width = pandas.Timedelta(days=args.max_width)
freq = args.freq
min_votes = args.min_votes

start_range = '1/1/2012'
end_range = '1/1/2015'
s = time.time()
intervals = interval.interval_set(start_range, end_range, freq=freq, max_delta=max_width, min_delta=min_width)
e = time.time()
print("interval time: ", str(e - s))
interval_params = [start_range, end_range, min_width, max_width, freq]
print("Interval space: ", len(intervals))

parameters = []
seen_list = []

conn = sqlite3.connect(':memory:')
dataframe.to_sql('o_data', conn)
c = conn.cursor()
d = conn.cursor()
c.execute("CREATE INDEX pid_index ON o_data(person_id)")
d.execute("CREATE TABLE vote_counts AS select count(1) as cnt, person_id, chamber FROM o_data group by person_id, chamber HAVING cnt >= 50")
d.execute("CREATE INDEX pid22_index on vote_counts(person_id)")
query = ("SELECT DISTINCT L.person_id as left, R.person_id"
         " AS right FROM vote_counts L, vote_counts R WHERE L.person_id != R.person_id"
         " AND L.person_id <= R.person_id AND L.chamber = R.chamber")

unique_pairs = c.execute(query).fetchall()
print(len(unique_pairs))
conn.close()

for left, right in unique_pairs:
    parameters.append((left, right, interval_params))

print("size of parameter space: ", len(parameters) * len(intervals))

"""unique_person_ids = dataframe['person_id'].unique()
print("unique people: " , len(unique_person_ids))
cnt=0
for left in unique_person_ids:
    chamber = dataframe[dataframe['person_id'] == left]
    chamber = chamber['chamber'].unique()
    for right in unique_person_ids: 
        if left != right and right not in seen_list:
            r_chamber = dataframe[dataframe['person_id'] == right]
            r_chamber = r_chamber['chamber'].unique()
            if chamber[0] == r_chamber[0]:
                parameters.append((left, right, interval_params))
    seen_list.append(left)
"""
print("Parameter space: ", len(parameters))
uuid = "vote_correlation"
log = utils.StatsLog(uuid)

t0 = time.time()
executor = DistributedExecutor(sparkContext, "fact-m", log)
check_context = GenericCheckComputeContext(executor, log=log, uuid=uuid, conf='solvent-sql.conf')

task = FixedMultiEpochTask(check_context, dataframe, parameters, epoch1=.25, epoch2=[.25, .25], log=log)
#results = task.compute(transform_parameters, calculate_voting, prune_by_count, sync_function = sync, mini_batch_size=100)
task.post_process(transform_parameters, calculate_voting)
t1 = time.time()
print("Total run time:", t1-t0)
log.dump_detailed_stats()
log.dump_aggregate_stats()


import csv
import check.permute.interval as interval 
import time
import datetime
import pandas as pd
import argparse
import sqlite3
import random

paulryan = 'R000570   '
maxinewaters = 'W000187   '
home_dir = '/home/vagrant/phd/politics/'
filename = '2012-curr-full-votes.csv'

#home_dir = '/mnt/factdb/'
#filename = 'full_votes.csv'

columns = ['vote_id', 'person_id', 'vote', 'category', 'chamber', 'session', 'date', 'number', 'subject', 'results', 'first_name', 'last_name', 'birthday', 'id_govtrack']
df = pd.read_csv(home_dir + filename, parse_dates=[2], header=None,index_col=False, names=columns)

df = df[['vote_id', 'person_id', 'vote', 'date', 'chamber']]
t0 = time.time()
parser = argparse.ArgumentParser()
parser.add_argument('--min_width', type=int, default=30)
parser.add_argument('--max_width', type=int, default=365)
parser.add_argument('--freq', type=str, default='15D')
parser.add_argument('--min_votes', type=int, default=50)
args = parser.parse_args()

min_width = pd.Timedelta(days=args.min_width)
max_width = pd.Timedelta(days=args.max_width)
freq = args.freq
min_votes = args.min_votes

start_range = '1/1/2012'
end_range = '1/1/2015'
s = time.time()
intervals = interval.interval_set(start_range, end_range, freq=freq, max_delta=max_width, min_delta=min_width)
e = time.time()
print("interval time: ", str(e - s))

print('Interval size: ' + str(len(intervals)))

conn = sqlite3.connect(':memory:')
df.to_sql('data', conn)
c = conn.cursor()
c.execute("CREATE INDEX idx1 ON data(person_id)")
c.execute("CREATE INDEX idx2 ON data(person_id, date)")

parameters = []

#c.execute("CREATE INDEX pid_index ON o_data(person_id)")
c.execute("CREATE TABLE vote_counts AS select count(1) as cnt, person_id, chamber FROM data group by person_id, chamber HAVING cnt >= 50")
c.execute("CREATE INDEX pid22_index on vote_counts(person_id)")
query = ("SELECT DISTINCT L.person_id as left, R.person_id"
         " AS right FROM vote_counts L, vote_counts R WHERE L.person_id != R.person_id"
         " AND L.person_id <= R.person_id AND L.chamber = R.chamber")

unique_pairs = c.execute(query).fetchall()
print(len(unique_pairs))
parameters = unique_pairs

query = ('SELECT left_vote, right_vote, left_date '
         'FROM (SELECT vote_id as left_vote_id, '
         'vote as left_vote, date as left_date '
         'FROM data where person_id = ?) LEFT_SIDE JOIN '
         '(SELECT vote_id as right_vote_id, '
         'vote as right_vote, date as right_date '
         'FROM data where person_id = ?) RIGHT_SIDE '
         'ON LEFT_SIDE.left_vote_id = RIGHT_SIDE.right_vote_id WHERE '
         'left_date >= DATE(?) AND left_date <= DATE(?)')
 
gen_end_time = time.time()
print("Gen time: ", str(gen_end_time - s))
#total_time = gen_end_time - gen_time
total_time = 0
curr_cnt = 0
random.shuffle(intervals)
for left, right in unique_pairs:
    for id, start, end in intervals:
        c = conn.cursor()
        query_start = time.time()
        results = c.execute(query, [left, right, start, end]).fetchall()
        agree = 0
        disagree = 0
        for left_vote, right_vote, left_date in results:
            if left_vote == right_vote:
                agree += 1
            else:
                disagree += 1
        if (agree + disagree) <= 50:
            rval = False
        else:
            rval = (agree) / (agree + disagree)
        
        query_end = time.time() 
        total_time += query_end - query_start
        curr_cnt += 1
        print("Query time: ", str(query_end - query_start))
        if curr_cnt % 50 == 0:
            print("Average time: ", str(total_time / float(curr_cnt)))
        c.close()
print("Total time: ", total_time)
print("Average time: ", total_time / float(len(intervals)))


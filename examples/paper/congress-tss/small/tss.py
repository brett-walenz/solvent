from pyspark import SparkContext
import csv
import check.permute.interval as interval 
import time
import datetime
import pandas as pd
import argparse
import check.utils as utils

from check.executor import DistributedExecutor
from check.context import cacheable, computes, inputs
from check.context import GenericCheckComputeContext
from check.task import FixedCacheSingleEpochTask

data_path = '~/phd/research/rip/research/unified/examples/'
df = pd.read_csv(data_path + 'data/votes.csv', parse_dates=[2], header=None,index_col=False, names=['a_vote', 'b_vote', 'date', 'number', 'session', 'chamber', 'result'])

@computes(["percent", "agreed", "disagreed"])
def calculate_percent(queryable, computation_parameters, cache_queryable):
    start, end = computation_parameters
    agreed = queryable.sql("SELECT count(1) as agreed FROM data WHERE a_vote = b_vote AND date >= ? AND date <= ?", params=(start, end))[0][0]
    disagreed = queryable.sql("SELECT count(1) as total FROM data WHERE a_vote != b_vote AND date >= ? AND date <= ?", params=(start, end))[0][0]
    if (agreed + disagreed == 0):
        disagreed = 1
    rval = True
    if (agreed / float(disagreed + agreed)) > .10:
        rval = False
    return (rval, (agreed/float(disagreed + agreed), agreed, disagreed))

@cacheable
@inputs(["id", "start", "end"])
@computes(["start", "end"], index='date')
def computation_parameters(data, parameters):
    id, interval_start, interval_end = parameters
    #start, end = data.sql("SELECT min(date), max(date) FROM data WHERE date >= ? AND date <= ?", (interval_start, interval_end))[0]
    start = data.sql("SELECT min(date) FROM data WHERE date >= ?", (interval_start,))[0][0]
    end = data.sql("SELECT max(date) FROM data WHERE date <= ?", (interval_end,))[0][0]
    return (start, end)
    
    #return (interval_start, interval_end)
 
def prune_by_percent(data, computation_parameters):
    start, end = computation_parameters
    agree, disagree = data.sql("SELECT COALESCE(SUM(agreed), 0), COALESCE(SUM(disagreed), 0) FROM data where (start >= ? AND end <= ?)", [start, end])[0]
    denom_sql = ("SELECT COALESCE(SUM(agreed), 0), COALESCE(SUM(disagreed), 0) FROM data WHERE (start >= ? AND end <= ?) "
                 "OR (start <= ? AND end >= ?) OR (start <= ? AND end >= ?)")
    all_agree, all_disagree = data.sql(denom_sql, [start, end, start, start, end, end])[0]
    if all_agree + all_disagree == 0:
        all_disagree = 1

    if agree / float(all_agree + all_disagree) > .10:
        return False
    
    if all_agree + disagree == 0:
        disagree = 1
#    print("START: ", str(start), " END: " , str(end), " all_agree: ", str(all_agree), " all_disagree: ", str(all_disagree), " agree: ", str(agree), " disagree: ", str(disagree))
    if all_agree / float(disagree + all_agree) <= .10:
        return True
    return 2 

t0 = time.time()
sparkContext = SparkContext("spark://fact-m:7077", "FactCheck")
parser = argparse.ArgumentParser()
parser.add_argument('--seed')
parser.add_argument('--param_width', type=int)
parser.add_argument('--max_width', type=int)
args = parser.parse_args()
print("ARGUMENTS")
print(args.seed)
print(args.param_width)

min_delta=pd.Timedelta(days=args.param_width)
max_delta=pd.Timedelta(days=args.max_width)

log = utils.StatsLog("Seed Width({0}), Min Gap({1}), Max Gap({2})".format(args.seed, args.param_width, args.max_width))
start_range = '1/1/2002'
end_range = '1/1/2012'
print('Interval Range(' + start_range + ' - ' + end_range + ')')
intervals = interval.interval_set(start_range, end_range, max_delta=max_delta, min_delta=min_delta)
print('Interval size: ' + str(len(intervals)))

executor = DistributedExecutor(sparkContext, "fact-m", log)
check_context = GenericCheckComputeContext(executor, log=log)
params = intervals

initial_params = interval.segmented_interval_set(start_range, end_range, '10D')

task = FixedCacheSingleEpochTask(check_context, df, params, initial_params, log=log)
results = task.compute(computation_parameters, calculate_percent, prune_by_percent)

t1 = time.time()
log.dump_detailed_stats()
log.dump_aggregate_stats()
print(t1-t0)
print(len(results))

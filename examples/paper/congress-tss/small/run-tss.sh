#!/bin/bash
(cd /home/vagrant/phd/research/rip/research/unified ; python setup.py bdist_egg)
redis-cli flushall
PW=15
S=15D
MAX=30
/home/hadoop/spark-install/bin/spark-submit --master spark://fact-m:7070 --py-files /home/vagrant/phd/research/rip/research/unified/dist/check-0.0.1-py2.7.egg tss.py --seed $S --param_width $PW --max_width $MAX > run-tss-$S-$PW.txt

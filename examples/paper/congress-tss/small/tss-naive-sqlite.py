import csv
import check.permute.interval as interval 
import time
import datetime
import pandas as pd
import argparse
import sqlite3

home_dir = '/home/vagrant/'
df = pd.read_csv(home_dir + 'phd/research/rip/research/unified/examples/data/votes.csv', parse_dates=[2], header=None,index_col=False, names=['a_vote', 'b_vote', 'date', 'number', 'session', 'chamber', 'result'])

t0 = time.time()
parser = argparse.ArgumentParser()
parser.add_argument('--seed')
parser.add_argument('--param_width', type=int)
parser.add_argument('--max_width', type=int)
args = parser.parse_args()

min_delta=pd.Timedelta(days=args.param_width)
max_delta=pd.Timedelta(days=args.max_width)

start_range = '1/1/2002'
end_range = '1/1/2012'
print('Interval Range(' + start_range + ' - ' + end_range + ')')
intervals = interval.interval_set(start_range, end_range, max_delta=max_delta, min_delta=min_delta)
print('Interval size: ' + str(len(intervals)))

conn = sqlite3.connect(':memory:')
df.to_sql('data', conn)
c = conn.cursor()

query = ("SELECT agreed/(agreed + disagreed + 1) as percent FROM "
        "(SELECT COALESCE(count(1), 0) as agreed FROM data WHERE "
        "a_vote = b_vote AND date >= ? AND date <= ?) AGREE, "
        "(SELECT COALESCE(count(1), 0) as disagreed FROM data WHERE "
        "a_vote != b_vote AND date >= ? AND date <= ?) DISAGREE ")
total_time = 0
for id, start, end in intervals:
    query_start = time.time()
    results = c.execute(query, [start, end, start, end]).fetchall()
    query_end = time.time() 
    total_time += query_end - query_start
    #print("Query time: ", query_end - query_start)

print("Total time: ", total_time)
print("Average time: ", total_time / float(len(intervals)))


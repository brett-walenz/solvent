#!/bin/bash
(cd /home/vagrant/phd/research/rip/research/unified ; python setup.py bdist_egg)
redis-cli flushall
/home/hadoop/spark-install/bin/spark-submit --master spark://fact-m:7070 --py-files /home/vagrant/phd/research/rip/research/unified/dist/check-0.0.1-py2.7.egg majority_vote_corr.py > run_out.txt

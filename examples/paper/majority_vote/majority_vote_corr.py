import check
import pandas

from pyspark import SparkContext
import check.permute.interval as interval
import time
import datetime
import argparse
import check.utils as utils
from check.executor import DistributedExecutor
from check.context import cacheable, computes, generative, inputs
from check.context import GenericCheckComputeContext
from check.task import FixedMultiEpochTask, PrecomputedMultiEpochTask, SingleEpochTask
from operator import itemgetter
import sqlite3
import random
from sqlalchemy import create_engine

def sync(cache_queryable, results, columns):
    values_string = ""
    for c in columns: 
        values_string += "?, "

    curs = cache_queryable.conn.connection.cursor()
    curs.executemany("INSERT INTO data VALUES (" + values_string[:-2] + ")", results)
    results = curs.execute("SELECT * FROM data WHERE (agree + disagree) <= 50")
    full_results = results.fetchall()
    curs.execute("DELETE FROM data")
    curs.executemany("INSERT INTO data VALUES (" + values_string[:-2] + ")", full_results)
    
    return cache_queryable

@computes(["percent", "agree", "disagree"])
def calculate_voting(queryable, (person_id, party, start, end), cache):
    query = ('SELECT cr_vote, mvr_vote, date FROM data WHERE '
            'person_id = :person_id AND date >= DATE(:start) AND date <= DATE(:end) AND '
            'party = :party')
    results = queryable.sql(query, person_id=person_id, start=start, end=end, party=party)
    agree = 0
    disagree = 0
    for cr_vote, mvr_vote, left_date in results:
        if cr_vote == mvr_vote:
            agree += 1
        else:
            disagree += 1
    percent = 0
    response = False
    if (agree + disagree) != 0:
        percent = agree / float((agree + disagree))
    if (agree + disagree) > 50:
        response = True 
    return (response, (percent, agree, disagree))
       
def prune_by_count(cache, (person_id, party, start, end)):
    query = ('SELECT count(1) FROM data WHERE t_person_id = :person_id'
            ' AND start_date <= DATE(:start) AND end_date >= DATE(:start) AND t_party=:party AND (agree + disagree) < 50')
    results = cache.sql(query, person_id=person_id, start=start, end=end, party=party).fetchone()[0]
    if results > 0:
        return 0
    return 2

@generative()
@inputs(["person_id", "party", "generated_params"])
@computes(["t_person_id", "t_party", "start_date", "end_date"], index='person_id')
def transform_parameters(queryable, (person_id, party, generated_params), memory):
    start_range, end_range, min_width, max_width, freq = generated_params
    key = str(start_range) + str(end_range) + str(min_width) + str(max_width) + str(freq)
    if not key in memory:    
        intervals = interval.interval_set(start_range, end_range, freq=freq, max_delta=max_width, min_delta=min_width)
        new_intervals = []
        #intervals = interval.segmented_interval_set(start_range, end_range, freq)
        for id, start, end in intervals:
            new_intervals.append((start, end, end-start))
        intervals = sorted(new_intervals, key=itemgetter(2), reverse=False)
        memory[key] = intervals
        print("key not in memory {0}".format(key))
    else:
        intervals = memory[key]
    query = "SELECT min(date) as min_date, max(date) as max_date FROM data where person_id = :person_id AND date >= DATE(:start) AND date <= DATE(:end)" 
    for start, end, _ in intervals:
         #print("PARAMETER: ", person_id)
         results = queryable.sql(query, person_id=person_id, start=start, end=end).fetchone()
         yield (person_id, party, results[0], results[1])
         #yield (person_id, party, start, end)

parser = argparse.ArgumentParser()
parser.add_argument('--min_width', type=int, default=15)
parser.add_argument('--max_width', type=int, default=365)
parser.add_argument('--freq', type=str, default='15D')
parser.add_argument('--min_votes', type=int, default=50)
args = parser.parse_args()

min_width = pandas.Timedelta(days=args.min_width)
max_width = pandas.Timedelta(days=args.max_width)
freq = args.freq
min_votes = args.min_votes

start_range = '1/1/2012'
end_range = '1/1/2015'
int_min_width = pandas.Timedelta(days=10)
int_max_width = pandas.Timedelta(days=365)
interval_params = [start_range, end_range, min_width, max_width, freq]
interval_params_2 = ['1/1/2012', '1/1/2013', int_min_width, int_max_width, '5D']
interval_params_3= ['1/1/2013', '1/1/2014', int_min_width, int_max_width, '5D']
interval_params_4= ['1/1/2014', '1/1/2015', int_min_width, int_max_width, '5D']

parameters = []
seen_list = []

engine = create_engine('postgresql+psycopg2://fact_user:lzc1123@fact-m/factdb')
c = engine.connect()
reps = c.execute("SELECT DISTINCT person_id FROM majority_vote_common_table").fetchall()

dataframe = pandas.read_sql("SELECT person_id, date, cr_vote, mvr_vote, party from majority_vote_common_table WHERE date >= '2012-01-01'", engine)

first_epoch = []
second_epoch = []
third_epoch = []
fourth_epoch = []
all_params = []
for rep in reps:
    
    first_epoch.append((rep[0], "Republican", interval_params))
    first_epoch.append((rep[0], "Democrat", interval_params))
    second_epoch.append((rep[0], "Republican", interval_params_2))
    second_epoch.append((rep[0], "Republican", interval_params_2))
    third_epoch.append((rep[0], "Democrat", interval_params_3))
    third_epoch.append((rep[0], "Democrat", interval_params_3))
    fourth_epoch.append((rep[0], "Republican", interval_params_4))
    fourth_epoch.append((rep[0], "Democrat", interval_params_4))

parameters = [first_epoch, second_epoch, third_epoch, fourth_epoch]
all_params.extend(first_epoch)
all_params.extend(second_epoch)
all_params.extend(third_epoch)
all_params.extend(fourth_epoch)
random.shuffle(all_params)
print(len(first_epoch))
print(len(second_epoch))
print(len(third_epoch))
print(len(fourth_epoch))
uuid = "majority_vote_correlation"
log = utils.StatsLog(uuid)

sparkContext = SparkContext("spark://fact-m:7077", "All Vote Correlation")
t0 = time.time()
executor = DistributedExecutor(sparkContext, "fact-m", log)
check_context = GenericCheckComputeContext(executor, log=log, uuid=uuid, conf='solvent-sql.conf')

#task = FixedMultiEpochTask(check_context, dataframe, first_epoch, log=log)
#task = PrecomputedMultiEpochTask(check_context, dataframe, parameters, log=log)
task = SingleEpochTask(check_context, dataframe, first_epoch, log=log)
results = task.compute(transform_parameters, calculate_voting, prune_by_count, sync_function = sync, mini_batch_size=100)
task.post_process(transform_parameters, calculate_voting)
t1 = time.time()
print("Total run time:", t1-t0)
log.dump_detailed_stats()
log.dump_aggregate_stats()


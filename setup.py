import os
from setuptools import setup, find_packages


setup(
        name = "check",
        version = "0.0.1",
        author = "Brett Walenz",
        author_email = "bwalenz@cs.duke.edu",
        description = ("Fact checking support library"),
        license = "BSD",
        packages = find_packages(),
        )


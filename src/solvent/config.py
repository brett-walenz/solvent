from store import SQLStore, JSONPostgresStore, NullStore
import utils
import query

def get_store(store_config, compute_function=None, params_function=None):
    store = None
    store_type = store_config.get('Store', 'type')
    if store_type == 'null':
        store = NullStore()
    elif store_type == 'sql':
        if compute_function and params_function:
            input_cols = utils.get_input_columns(params_function)
            trans_cols = utils.get_transform_columns(params_function)
            comp_cols = utils.get_compute_columns(compute_function)
        store = SQLStore(store_config.get('Store', 'alchemy_conn'), 
                store_config.get('Store', 'output_table'),
                fact_table=store_config.get('Store', 'temp_fact_table'),
                params_table=store_config.get('Store', 'temp_params_table'),
                input_columns=input_cols, params_columns=trans_cols,
                fact_columns=comp_cols)
    elif store_type == 'pg-json':
        store = JSONPostgresStore(store_config.get('Store', 'alchemy_conn'), 
            store_config.get('Store', 'fact_type'))
    return store

def get_queryable(store_config, data, data_columns, index):
    queryable = None
    query_type = store_config.get('Queryable', 'type')
    if query_type == 'in-memory':
        queryable = query.Queryable(data, data_columns, index=index)
    elif query_type == 'remote':
        queryable = query.RemoteQueryable(store_config.get('Queryable', 'alchemy_conn'))
    return queryable

def get_parallelism(store_config):
    parallelism = int(store_config.get('General', 'parallelism'))
    return parallelism

def use_distributed_cache(store_config):
    cache = store_config.getboolean('General', 'distributed_cache')
    return cache

def use_pruning_function(store_config):
    prune = store_config.getboolean('General', 'pruning_function')
    return prune


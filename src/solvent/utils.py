NEED_EVAL = 2
import datetime
import random
import numpy
import csv
import collections 

class EpochStats(object):
    def __init__(self):
        self.auto_include = 0
        self.need_eval = 0
        self.discard = 0
        self.pspace_size = 0
        self.run_time = 0
        self.transform_time = 0
        self.pruning_time = 0
        self.computation_time = 0
        self.settings = "None"
        self.sample_size = 0
        self.sync_time = 0
        self.mini_batch_size = 0
        self.cache_size = 0
        self.computation_length = 0
        self.bundle_time = 0

    def as_array(self, as_numpy=False):
        row = [self.run_time, self.transform_time, self.pruning_time, self.computation_time, self.pspace_size, self.auto_include, self.need_eval, self.discard, self.sample_size, self.sync_time, self.mini_batch_size, self.cache_size, self.computation_length, self.bundle_time]
        if as_numpy:
            row = numpy.array(row)
        return row

class StatsLog(object):
    def __init__(self, settings_str, filename='runs.csv', aggregate_filename='agg_runs.csv'):
        self.filename = filename
        self.aggregate_filename=aggregate_filename
        self.epochs = []
        self.current_epoch = EpochStats() 
        self.settings = settings_str 

    def set_settings(self, parameter_settings):
        self.settings = parameter_settings

    def set_run_time(self, run_time):
        self.current_epoch.run_time = run_time

    def set_epoch_stats_and_times(self, stats_lists, times_lists, bundle_lists):
        prune_statistics = prune_stats(stats_lists)
        timing_statistics = timing_stats(times_lists)
        bundle_statistics = bundle_stats(bundle_lists)
        self.set_epoch_stats(*prune_statistics)
        self.set_epoch_times(*timing_statistics)
        self.set_epoch_bundle_stats(*bundle_statistics)

    def set_pspace_size(self, pspace_size):
        self.current_epoch.pspace_size = pspace_size

    def set_epoch_stats(self, auto_include, need_eval, discard):
        self.current_epoch.auto_include = auto_include
        self.current_epoch.need_eval = need_eval
        self.current_epoch.discard = discard

    def set_sample_size_decimal(self, decimal_percent):
        self.current_epoch.sample_size = decimal_percent

    def set_epoch_times(self, transform_time, prune_time, computation_time, sync_time):
        self.current_epoch.transform_time = transform_time
        self.current_epoch.prune_time = prune_time
        self.current_epoch.computation_time = computation_time
        self.current_epoch.sync_time = sync_time

    def set_epoch_bundle_stats(self, computation_length, bundle_time):
        self.current_epoch.computation_length = computation_length
        self.current_epoch.bundle_time = bundle_time

    def set_epoch_run_time(self, run_time):
        self.current_epoch.run_time = run_time

    def set_sync_time(self, sync_time):
        self.current_epoch.sync_time = sync_time

    def set_mini_batch_size(self, mini_batch_size):
        self.current_epoch.mini_batch_size = mini_batch_size

    def set_cache_size(self, cache_size):
        self.current_epoch.cache_size = cache_size

    def start_next_epoch(self, run_time=None):
        if run_time:
            self.set_run_time(run_time)
        self.epochs.append(self.current_epoch)
        self.current_epoch = EpochStats()

    def end(self):
        if self.current_epoch:
            self.epochs.append(self.current_epoch)
            self.current_epoch = None

    def dump_detailed_stats(self):
        self.end()
        with open(self.filename, 'a') as csvfile:
            csvwriter = csv.writer(csvfile)
            for index, epoch_stats in enumerate(self.epochs):
                row = [self.settings, index] + epoch_stats.as_array()
                csvwriter.writerow(row)

    def dump_aggregate_stats(self):
        self.end()
        with open(self.aggregate_filename, 'a') as csvfile:
            csvwriter = csv.writer(csvfile)
            agg = numpy.array([]) 
            for index, epoch_stats in enumerate(self.epochs):
                row = epoch_stats.as_array(as_numpy=True)
                if not agg.any():
                    agg = row
                else:
                    agg = agg + row
            agg = [self.settings] + agg.tolist()
            csvwriter.writerow(agg)
           

def prune_stats(stats_list):
    a, k, d = 0, 0, 0
    if not stats_list:
        return (0, 0, 0)
    for auto, keep, discard in stats_list:
        a += auto
        k += keep
        d += discard 

    return (a, k, d)

def timing_stats(stats_list):
    a, k, d, s = 0, 0, 0, 0
    if not stats_list:
        return (0, 0, 0, 0)
    for transform, prune, compute, sync in stats_list:
        a += transform 
        k += prune 
        d += compute
        s += sync

    return (a, k, d, s)

def bundle_stats(stats_list):
    len_cl, bundle_time = 0, 0
    if not stats_list:
        return (0, 0)
    for cl, bt in stats_list:
        len_cl += cl
        bundle_time += bt
    return (len_cl, bundle_time)


def sample_and_remove(parameter_set, sample_rate=.05):
    original_indices = range(0, len(parameter_set))
    indices = random.sample(original_indices, int(len(parameter_set) * sample_rate))
    sample = [parameter_set[i] for i in indices]
    keep_indices = set(original_indices).difference(indices)
    reduced_parameter_set = [parameter_set[i] for i in keep_indices]
    return sample, reduced_parameter_set 

def results_list_flattened(results):
    frame_data = []
    for entry in results:
            key, parameter, result = entry
            row = []
            row.extend(parameter)
            if isinstance(result, list) or isinstance(result, tuple):
                row.extend(result)
            else:
                row.append(result)
            frame_data.append(row)
    return frame_data
    

def results_as_rows(results, params_func, computation_func):
    frame_data = []
    if computation_func.func_dict["columns"]:
        columns = params_func.func_dict["columns"]
        columns.extend(computation_func.func_dict["columns"])
        frame_data = results_list_flattened(results)
    return (frame_data, columns) 

def get_transform_dict(params, params_func):
    return get_compute_dict(params, params_func)

def get_compute_dict(compute_results, computation_func):
    if isinstance(compute_results, collections.Iterable):
        return dict(zip(computation_func.func_dict["columns"], compute_results))
    else:
        col = computation_func.func_dict["columns"][0]
        return dict([(col, compute_results)])

def get_query_dict(query_params, params_func):
    if isinstance(query_params, collections.Iterable): 
        return dict(zip(params_func.func_dict["inputs"], query_params))
    else:
        return dict([(params_func.func_dict["inputs"][0], query_params)])

def get_input_columns(params_func):
    return params_func.func_dict["inputs"]

def get_transform_columns(params_func):
    return params_func.func_dict["columns"]

def get_compute_columns(computation_func):
    return computation_func.func_dict["columns"]

def get_cache_columns(params_func, computation_func):
    columns = []
    columns.extend(params_func.func_dict["columns"])
    columns.extend(computation_func.func_dict["columns"])
    return columns





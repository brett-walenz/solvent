class ParameterGroup(object):

    def __init__(self, input_values = None):
        self.input = input_values
        self.transform = None
        self.output = None
        self.accept = False

    def set_input(self, input_values):
        self.input = input_values

    def set_transform(self, transform_values):
        self.transform = transform_values

    def set_output(self, output_values, accept=True):
        self.output = output_values
        self.accept = accept

    def get_input(self):
        return self.input

    def get_transform(self):
        return self.transform

    def get_output(self):
        return self.output


class Bundle(object):

    def __init__(self, key=None, result=None):
        self.key = key
        self.bundle = [] 
        self._result = result 

    def add_to_bundle(self, value):
        self.bundle.append(value)

    def init(self, key):
        ''' This function should be used in conjunction with defaultdict '''
        if not self.key:
            self.key = key
            self.bundle = []

    @property
    def result(self):
        return self._result
   
    @result.setter
    def result(self, r):
        self._result = r 
   
    def __str__(self):
        return str(len(self.bundle)) + "::" + str(self.bundle[0]) + "::" + self.key 

def as_parameter_list(parameter_bundles):
    bundle_list = []
    for b in parameter_bundles:
        bundle_list.append((b.key, b.bundle[0]))
    return bundle_list

import redis
import pandas
from functools import partial 
import cache as cache
import query as query
import numpy
import math

from utils import NEED_EVAL
import utils as utils
import random
import time
from store import NullStore
from store import JSONPostgresStore
from store import SQLStore
from params import ParameterGroup 
import config
import collections
import debug

def _make_string_from_tuple(tup):
    if not isinstance(tup, collections.Iterable):
        return str(tup)
    s = ""
    for value in tup:
        s += ":" + str(value)
    return s

def _check_global_cache_for_computation(computation_list, g_cache, uuid, parameter_group):
    ''' Returns a computation_list, a collection of ParameterGroup objects.
    ParameterGroup's are one to many mappings of input parameters to computation (transform) parameters.
    '''
    skip = 0
    if parameter_group:
        key = _make_string_from_tuple(parameter_group.get_transform())
        if not g_cache.bundle_exists(key, uuid):
            #parameter_group.set_transform(parameter_group.get_transform())
            computation_list.append(parameter_group)
            #print("Key does not exist: {0}".format(key))
        else:
            skip = 1

        g_cache.add_to_bundle(key, uuid, parameter_group.get_input())
    return computation_list, skip

def cacheable_params_executor(queryable, parameter_sets, params_function, uuid, g_cache, store, memory, distributed_cache=True):
    start = time.time()
    computation_list = []
    skipped = 0
    cnt = 0
    mint = 0
    maxt = 0
    for ps in parameter_sets:
        sp = time.time()
        parameters_key = _make_string_from_tuple(ps)
        params_dict = utils.get_query_dict(ps, params_function)
        if 'generative' in params_function.func_dict:
            for computation_set in params_function(queryable, ps, memory):
                group = ParameterGroup(ps)
                group.set_transform(computation_set)
                transform_key = _make_string_from_tuple(group.get_transform())
                if distributed_cache:
                    computation_list, skip = _check_global_cache_for_computation(computation_list, g_cache, uuid, group)
                    if not skip:
                        store.add_query(params_dict, transform_key, parameters_key)
                else: 
                    computation_list.append(group)
                    skip = 0
                skipped += skip 
        else: 
            computation_set = params_function(queryable, ps, memory)
            group = ParameterGroup(ps)
            group.set_transform(computation_set)
            transform_key = _make_string_from_tuple(group.get_transform())
            store.add_query(params_dict, transform_key, parameters_key)
            if distributed_cache:
                computation_list, skip = _check_global_cache_for_computation(computation_list, g_cache, uuid, group)
            else:
                computation_list.append(group)
                skip = 0
            skipped += skip 
        ep = time.time()
        if ep - sp < mint:
            mint = ep - sp
        elif ep - sp > maxt:
            maxt = ep - sp
    end = time.time()
    print("Batch parameter time: ", str(end-start))
    print("Min: ", str(mint), " Max: ", str(maxt))
    print("Computations: {0}, Bundled: {1}".format(len(computation_list), skipped))
    return (computation_list, end - start, (len(computation_list), skipped))

def params_executor(queryable, parameter_sets, params_function, uuid, g_cache):
    start = time.time()
    computation_list = []
    for ps in parameter_sets:
        computation_set = params_function(queryable, ps)
        if computation_set:
            key = _make_string_from_tuple(computation_set)
            computation_list.append(computation_set)
            g_cache.add_to_bundle(key, uuid, ps)
    end = time.time()
    #print("Batch parameter time: ", str(end-start))
    return (computation_list, end - start)

def prune_executor(cache, computation_params, prune_function):
    auto = []
    keep = []
    start = time.time()
    need_eval, auto_inc, discard = 0, 0, 0
    for group in computation_params:
        value = group.get_transform()
        key = _make_string_from_tuple(value)
        result = prune_function(cache, value)
        if result == 2:
            keep.append((key, group))
            need_eval += 1
        elif result:
            auto.append((group))
            auto_inc += 1
        else:
            discard += 1
    end = time.time()
    #print("Batch prune time: ", str(end-start))
    stats = [auto_inc, need_eval, discard]
    return (auto, keep, stats, end - start)

def compute_executor(data, parameter_sets, cache, store, function=None, store_rejects=False, param_func=None):
    accepts = []
    rejects = []
    start = time.time()
    store_rejects = False
    #key is a list of strings identifying each parameter
    for group in parameter_sets:
        value = group.get_transform()
        key = _make_string_from_tuple(value)
        input_key = _make_string_from_tuple(group.get_input())
        (accept, rval) = function(data, value, cache)
        group.set_output(rval, accept)
        
        if accept:
            compute_dict = utils.get_compute_dict(rval, function)
            params_dict = utils.get_transform_dict(value, param_func)
            store.add_computation(params_dict, compute_dict, key, input_key)
            accepts.append((key, group))
        else:
            if store_rejects:
                compute_dict = utils.get_compute_dict(rval, function)
                params_dict = utils.get_transform_dict(value, param_func)
                store.add_computation(params_dict, compute_dict, key, input_key)
            rejects.append((key, group))
    end = time.time()
    #print("Batch compute time: ", str(end-start))
    return (accepts, rejects, end - start)

def cache_update_executor(sync_function, queryable_cache, results):
    start = time.time()
    results_list = []
    for key, group in results:
        if not isinstance(group.get_transform(), collections.Iterable):
            c = list([group.get_transform()])
        else:
            c = list(group.get_transform())
        if isinstance(group.get_output(), collections.Iterable):
            c.extend(group.get_output())
        else:
            c.append(group.get_output())
        results_list.append(c)
    queryable_cache.local_sync(sync_function, results_list) 
    end = time.time()
    return (queryable_cache, end - start)

def compute_wrapper(((key, l_cache), parameter_sets), broadcast_data=None, params_function=None, compute_function=None, data_columns=None, cache_columns=None, prune_function=None, uuid="default", redis_host="localhost", mini_batch_size=100, sync_function=None, store=None, collect=False, conf=None):
    data = broadcast_data.value
    memory = dict()
    store = config.get_store(conf, params_function=params_function, compute_function=compute_function)
    g_cache = cache.GlobalCache(redis_host)
    print("Parameter sets received of length {0}".format(len(parameter_sets)))
    print(parameter_sets)
    queryable_data = config.get_queryable(conf, data, data_columns, params_function.func_dict['index'])
    data = None
    broadcast_data = None
    stats = [0, 0, 0]
    bundle_stats = [0, 0]
    prune_time_t = 0
    compute_time_t = 0
    transform_time_t = 0
    sync_time = 0
    computed_results = []
    inner_batch_size = mini_batch_size
    #split the computation into mini batches
    if not mini_batch_size or mini_batch_size > len(parameter_sets):
        mini_batch_size = len(parameter_sets)
    iterations = int(math.ceil(len(parameter_sets) / float(mini_batch_size)))
    queryable_cache = query.ComputationCache(l_cache, cache_columns)
    for i in range(iterations):
        iter_time = time.time()
        batch_start = i * mini_batch_size
        batch_parameters = parameter_sets[batch_start:batch_start + mini_batch_size]
        print("Parameters of batch length: {0}".format(len(batch_parameters)))
        if len(memory.keys()) > 100:
            memory = dict()
        use_d_cache = config.use_distributed_cache(conf)
        parameter_groups, params_time, batch_bundle_stats = cacheable_params_executor(queryable_data, batch_parameters, params_function, uuid, g_cache, store, memory, distributed_cache=use_d_cache) 
        bundle_stats = numpy.add(bundle_stats, batch_bundle_stats)
        transform_time_t += params_time
        print("Generated {0} parameter groups".format(len(parameter_groups)))
        #if transform is generative, our computation params could be much 
        #larger, so split it into further batches
        if not inner_batch_size or inner_batch_size > len(parameter_groups):
            inner_batch_size = len(parameter_groups)
        inner_iterations = 1
        if 'generative' in params_function.func_dict and inner_batch_size:
            #random.shuffle(parameter_groups)
            inner_iterations = int(math.ceil(len(parameter_groups) / float(inner_batch_size)))
        #print("Inner Iterations: ", inner_iterations)
        for j in range(inner_iterations):
            #queryable_cache.clear()
            inner_s_t = time.time()
            prune_time = 0
            inner_batch_start = j * inner_batch_size
            inner_batch_parameters = parameter_groups[inner_batch_start:inner_batch_start + inner_batch_size]
            bts = inner_batch_start + inner_batch_size
            if not queryable_cache.is_blank() and config.use_pruning_function(conf):
                auto, keep, batch_stats, prune_time = prune_executor(queryable_cache, inner_batch_parameters, prune_function)
                stats = numpy.add(stats, batch_stats)
                reduced_params = []
                for key, group in keep:
                    reduced_params.append(group)
                inner_batch_parameters = reduced_params
                prune_time_t += prune_time

            accepts, rejects, compute_time = compute_executor(queryable_data, inner_batch_parameters, queryable_cache, store, compute_function, param_func=params_function) 
            compute_time_t += compute_time
            if (len(accepts) + len(rejects) > 0) and inner_batch_size:
                results = [] 
                results.extend(accepts)
                results.extend(rejects)
                queryable_cache, sync_time = cache_update_executor(sync_function, queryable_cache, results)
            if collect: 
                computed_results.extend(accepts)
                computed_results.extend(rejects)
            inner_e_t = time.time()
    queryable_data.close()
    store.flush()
    q_cache = queryable_cache.get_contents()
    #q_cache = []
    return (computed_results, q_cache, stats, (transform_time_t, prune_time_t, compute_time_t, sync_time), bundle_stats)
        
class DistributedExecutor(object):
    def __init__(self, context, redis_host, log):
        self.context = context
        self.redis_host = redis_host 
        self.g_cache = cache.GlobalCache(redis_host) 
        self.log = log

    def _parallelize_parameters(self, parameter_list, num_splits):
        split = len(parameter_list) / num_splits 
        split_parameters = []
        if split < 1:
            split = 1
        for i in range(0, len(parameter_list), split):
            print("Creating parameters: ", i, " :: ", i+split, len(parameter_list[i:i+split]))
            split_parameters.append(parameter_list[i:i+split])
            
        parameters_rdd = self.context.parallelize(split_parameters, num_splits)
        return parameters_rdd

    def _join_parameters_to_data(self, data, split_parameters):
        glom_data = data.glom()
        glom_param_rdd = glom_data.cartesian(split_parameters)
        return glom_param_rdd 

    def _parallelize_frame(self, frame):
        data_list = frame.values.tolist()
        columns = list(frame)
        data = self.context.parallelize(data_list, 1)
        data = data.glom()
        return (data, columns)

    def distributed_compute(self, joined_data, broadcast_data, data_columns, cache_columns, params_function, compute_function, prune_function=None, sync_function=None, mini_batch_size=100, conf=None):
        print("Creating worker function...")
        worker_function = partial(compute_wrapper, broadcast_data=broadcast_data, params_function=params_function, compute_function=compute_function, data_columns=data_columns, cache_columns=cache_columns, prune_function=prune_function, uuid=self.uuid, redis_host=self.redis_host, sync_function=sync_function, mini_batch_size=mini_batch_size, conf=conf)

        results = joined_data.map(worker_function, preservesPartitioning=True)
        print("Creating mapper, calling collect, batch size: ", mini_batch_size)
        values = results.collect()
        global_stats = []
        global_results = []
        global_timing = []
        global_bundle = []
        unified_cache_set = set()
        print("Finishing work.")
        for result_set, cache_set, stats, timing, bundle_stats in values:
            global_stats.append(stats)
            global_results.append(result_set)
            global_timing.append(timing)
            global_bundle.append(bundle_stats)
            unified_cache_set.update(cache_set)

        comps = [item for sublist in global_results for item in sublist]
        print("Unified cache size: ", len(unified_cache_set))
        return comps, unified_cache_set, global_stats, global_timing, global_bundle

    def compute(self, broadcast_data, data_columns, parameter_sets, params_function, compute_function, prune_function=None, cache=None, sync_function=None, mini_batch_size=100, conf=None):
        #data_rdd, data_columns = self._parallelize_frame(data)
        #data_columns = list(data)
        #broadcast_data = self.context.broadcast(data.values)
        print("Cache size before compute: ", cache.size())
        cache_rdd = cache.parallelize(self.context)
        #joined_rdd = data_rdd.cartesian(cache_rdd)
        #joined_rdd = joined_rdd.map(lambda x: (1, x))
        joined_rdd = cache_rdd.map(lambda x: (1, x))

        #now join with the parameter_sets
        split_parameters = self._parallelize_parameters(parameter_sets, config.get_parallelism(conf))
        joined_data = joined_rdd.cartesian(split_parameters)

        #now call our combined transform/prune/compute function
        comps, unified_cache, stats, timing, bundle_stats = self.distributed_compute(joined_data, broadcast_data, data_columns, cache.columns, params_function, compute_function, prune_function, sync_function, mini_batch_size=mini_batch_size, conf=conf)
    
        prune_statistics = utils.prune_stats(stats)
        timing_statistics = utils.timing_stats(timing)
        bundle_statistics = utils.bundle_stats(bundle_stats)
        self.log.set_epoch_stats_and_times(stats, timing, bundle_stats)
        print("Epoch finished.")
        print("Prune statistics: auto({0}), eval({1}), discard({2})".format(prune_statistics[0], prune_statistics[1], prune_statistics[2]))
        print("Timing statistics: parameter transform({0}), pruning({1}), computation({2})".format(timing_statistics[0], timing_statistics[1], timing_statistics[2]))
        return (comps, unified_cache, prune_statistics, timing_statistics, bundle_statistics)
    

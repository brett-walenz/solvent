#import permute.interval as interval
import check.utils as utils
import time
import sync
from store import SQLStore
import config

class Task(object):
    def __init__(self, context, dataframe, parameter_set, log=None):
        self.context = context
        self.dataframe = dataframe
        self.parameter_set = parameter_set
        self.log = log 

    def execute_epoch(self, parameters, parameters_function, computation_function, prune_function, sync_function=None, mini_batch_size=100, sync_at_end=True, pspace_size=0, final=False):
        start = time.time()
        epoch_results, epoch_cache, epoch_prune, epoch_time = self.context.compute(self.dataframe, parameters, parameters_function, computation_function, prune_function, as_bundle=False, sync_function=sync_function, mini_batch_size=mini_batch_size)
        if sync_at_end:
            self.context.sync_cache(epoch_results, epoch_cache, sync_function, self.context.local_cache)
        end = time.time()
        self.log.set_mini_batch_size(mini_batch_size)
        self.log.set_sample_size_decimal(pspace_size)
        self.log.set_cache_size(self.context.local_cache.size())
        if not final:
            self.log.start_next_epoch(end-start)
        else:
            self.log.set_epoch_run_time(end-start)
        return epoch_results, epoch_cache, epoch_prune, epoch_time

    def post_process(self, params_function, compute_function):
        store = config.get_store(self.context.conf, params_function=params_function, compute_function=compute_function)
        store.post_process()

class PrecomputedMultiEpochTask(Task):
    def __init__(self, context, dataframe, parameter_sets, log=None):
        super(PrecomputedMultiEpochTask, self).__init__(context, dataframe, parameter_sets, log)

    def compute(self, parameters_function, computation_function, prune_function=None, sync_function=None, mini_batch_size=100):
        computed_results = []
        for parameters in self.parameter_set:
            print("Executing an epoch with size: {0}".format(len(parameters)))
            epoch_results, _, _, _ = self.execute_epoch(parameters, parameters_function, computation_function, prune_function, sync_function=sync_function, mini_batch_size=mini_batch_size, pspace_size=len(parameters))
        return computed_results


class FixedMultiEpochTask(Task):
    def __init__(self, context, dataframe, parameter_set, epoch1=.01, epoch2=None, log=None):
        super(FixedMultiEpochTask, self).__init__(context, dataframe, parameter_set, log)
        self.epoch1 = epoch1
        self.epoch2 = epoch2
        self.postfix = "_" + str(self.epoch1) + "_"
        if not epoch2:
            self.epoch2 = []
        for epoch in self.epoch2:
            self.postfix += str(epoch) + "_"

    def compute(self, parameters_function, computation_function, prune_function=None, sync_function=None, mini_batch_size=100):
        #epoch 1
        computed_results = []
        seed_parameters, reduced_parameters = self.sample(self.parameter_set, self.epoch1)
        epoch_results, _, _, _ = self.execute_epoch(seed_parameters, parameters_function, computation_function, prune_function, sync_function, mini_batch_size=mini_batch_size, pspace_size=self.epoch1)
        computed_results.extend(epoch_results)

        #iterative epochs
        if self.epoch2:
            for epoch in self.epoch2:
                print("batch size: ", mini_batch_size)
                epoch_parameters, reduced_parameters = self.sample(reduced_parameters, epoch)
                epoch_results, _, _, _ = self.execute_epoch(epoch_parameters, parameters_function, computation_function, prune_function, sync_function=sync_function, mini_batch_size=mini_batch_size, pspace_size=epoch)
                computed_results.extend(epoch_results)
                print("Computation cache is size ", self.context.local_cache.size())

        #last epoch
        epoch_results, _, _, _ = self.execute_epoch(reduced_parameters, parameters_function, computation_function, prune_function, sync_function=sync_function, mini_batch_size=mini_batch_size, final=True)
        computed_results.extend(epoch_results)
        return computed_results 
 

    def sample(self, parameter_sets, rate=.05):
        return utils.sample_and_remove(parameter_sets, rate)

class SingleEpochTask(Task):
    def __init__(self, context, dataframe, parameter_set, log=None):
        super(SingleEpochTask, self).__init__(context, dataframe, parameter_set, log)
        self.postfix = "_single"

    def compute(self, parameters_function, computation_function, prune_function=None, sync_function=None, mini_batch_size=None):
        print("Single compute task started.")
        computed_results, _, _, _ = self.execute_epoch(self.parameter_set, parameters_function, computation_function, prune_function, sync_function, mini_batch_size, pspace_size=1)
        return computed_results 


class FixedCacheSingleEpochTask(Task):
    def __init__(self, context, dataframe, parameter_set, cache_parameter_set, log=None):
        super(FixedCacheSingleEpochTask, self).__init__(context, dataframe, parameter_set, log)
        self.postfix="_cache_single"
        self.cache_parameter_set = cache_parameter_set

    def compute(self, parameters_function, computation_function, prune_function=None, sync_function=sync.no_op, mini_batch_size=100):
        cache_results , _, _, _ = self.execute_epoch(self.cache_parameter_set, parameters_function, computation_function, prune_function, sync.add_all, mini_batch_size, pspace_size=1)
        computed_results, _, _, _ = self.execute_epoch(self.parameter_set, parameters_function, computation_function, prune_function, sync_function, mini_batch_size, pspace_size=1, final=True)
        return computed_results

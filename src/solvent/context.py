import redis
import abc
import bundle
import pickle
import numpy
import cache
import random
import pandas
import utils
import query
import ConfigParser

def computes(column_list, index=None):
    def computes_decorator(function):
        function.func_dict['columns'] = column_list
        function.func_dict['index'] = index
        return function
    return computes_decorator

def inputs(column_list):
    def inputs_decorator(function):
        function.func_dict['inputs'] = column_list
        return function
    return inputs_decorator

def generative():
    def generates_decorator(function):
        function.func_dict['generative'] = True
        return function
    return generates_decorator

def cacheable(function):
    function.func_dict['cacheable'] = True
    return function

class GenericCheckComputeContext:
    __metaclass__ = abc.ABCMeta

    def __init__(self, executor_context, redis_host='localhost', log=None, uuid="uncategorized", conf=None):
        self.g_cache = cache.GlobalCache(redis_host)
        self.uuid = uuid 
        self.executor_context = executor_context
        self.executor_context.uuid = self.uuid
        self.log = log
        self.local_cache = query.ComputationCache()
        if conf:
            self.conf = ConfigParser.ConfigParser()
            self.conf.readfp(open(conf))

    def bundle(self, data, parameter_sets, key_function):
        for key, parameter in self.executor_context.bundle(data, parameter_sets, key_function):
            self.g_cache.add_to_bundle(key, self.uuid, parameter)
        return self.g_cache.get_bundles(self.uuid)
    
    def compute(self, data, parameter_sets, params_function, compute_function, prune_function=None, as_bundle=False, sync_function=None, mini_batch_size=100, **kwargs):
        print("Starting compute task - parameter space(", len(parameter_sets), ")")
        self.log.set_pspace_size(len(parameter_sets))

        data_columns = list(data)
        broadcast_data = self.executor_context.context.broadcast(data.values)
        results = []
        if self.local_cache.is_blank():
            self.local_cache.columns = utils.get_cache_columns(params_function, compute_function)
        comps, cache, prune_stats, time_stats, bundle_stats =  self.executor_context.compute(broadcast_data, data_columns, parameter_sets, params_function, compute_function, prune_function, self.local_cache, sync_function, mini_batch_size=mini_batch_size, conf=self.conf)
        if as_bundle:
            for key, group in comps:
                b = self.g_cache.get_bundle(key, self.uuid)
                b.result = group.get_output() 
                results.append(b)
        else:
            results = comps
        return (results, cache, prune_stats, time_stats) 

    def sync_cache(self, results, unified_cache_list, sync_function, current_cache):
        print("Syncing cache: results {0}, unified cache list {1}".format(len(results), len(unified_cache_list)))
        self.local_cache = current_cache.global_sync(sync_function, unified_cache_list, results)

   
    def sample(self, data, params, params_func, computation_func, rate=.05, cache=True):
        """ Sample the parameters, then transform the params to computation parameters, then run the computation
            function. Cache the results afterward. """
        indices = random.sample(range(len(params)), int(len(params) * .05))
        sampled_params = [params[i] for i in indices]
        if cache:
            results, _, _ = self.cache(data, sampled_params, params_func, computation_func)
        return sampled_params 

    def prune(self, parameter_bundles, prune_function):
        bundles = []
        for key, value in self.executor_context.prune(parameter_bundles, prune_function, self.local_cache):
            b = self.g_cache.get_bundle(key, self.uuid)
            bundles.append(b)
        return bundles
    


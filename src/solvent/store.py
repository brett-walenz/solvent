from abc import ABCMeta, abstractmethod
import factdb
import psycopg2
from sqlalchemy import create_engine
import pandas
from collections import defaultdict
import time

class ResultsStore(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def insert_computations(self, results_list):
        pass

    @abstractmethod
    def add_computation(self, parameters_dict, result_dict, group_key, parameter_key):
        pass
    
    @abstractmethod
    def add_query(self, parameters_dict, group_key, parameters_key):
        pass

    @abstractmethod
    def insert_queries(self, query_list):
        pass

    @abstractmethod
    def flush(self):
        pass

class NullStore(ResultsStore):
    def insert_computations(self, results_list):
        return
    
    def add_query(self, parameters_dict, group_key, parameters_key):
        pass

    def add_computation(self, parameters_dict, result_dict, group_key, parameter_key):
        pass

    def insert_queries(self, query_list):
        pass

    def flush(self):
        pass

class JSONPostgresStore(ResultsStore):

    def __init__(self, conn_string, fact_type, flush_size=1000):
        self.flush_size = flush_size
        self.fact_type = fact_type
        self.curr_results = []
        self.query_params = []
        self.engine = create_engine(conn_string)
        conn = self.engine.connect()
        self.conn = conn.connection

    def insert_computations(self, results_list):
        factdb.insert_facts(self.conn, results_list)
        self.curr_results = []

    def add_computation(self, parameters_dict, result_dict, group_key, parameter_key):
        self.curr_results.append((self.fact_type, parameters_dict, result_dict, group_key, parameter_key))
        if len(self.curr_results) > self.flush_size:
            self.insert_computations(self.curr_results)

    def add_query(self, parameters_dict, group_key, parameters_key):
        self.query_params.append((self.fact_type, parameters_dict, parameters_key))
        if len(self.query_params) > self.flush_size:
            self.insert_queries(self.query_params)

    def insert_queries(self, query_list):
        factdb.insert_queries(self.conn, query_list)
        self.query_params = []

    def post_process(self):
        POST = ("INSERT INTO fact_table(type, compute_params, result, "
                "input_params, group_key, params_key) ( "
                "SELECT facts.type, facts.parameters as compute_params, "
                "facts.result, parameters.parameters as input_params, "
                "facts.group_key, facts.parameters_key FROM facts, parameters "
                "WHERE facts.parameters_key = parameters.parameters_key)")
        cursor = self.conn.cursor()
        cursor.execute(POST)
        self.conn.commit() 

    def flush(self):
        if len(self.curr_results) > 0:
            self.insert_computations(self.curr_results)
        if len(self.query_params) > 0:
            self.insert_queries(self.query_params)

class SQLStore(ResultsStore):

    def __init__(self, conn_string, output_table="gen_facts", input_columns=None, fact_columns=None, params_columns=None, fact_table="facts", params_table="params"):
        self.conn_string = conn_string
        self.engine = create_engine(conn_string)
        self.df = None
        self.params_df = None
        self.fact_table = fact_table
        self.params_table = params_table
        self.fact_columns = fact_columns 
        self.params_columns = params_columns
        self.input_columns = input_columns
        self.table_name = output_table

    def _add_dict_entries_to_frame(self, frame, dict_list, group_key=None, parameter_key=None):
        temp_dict = None
        if len(dict_list) > 1:
            temp_dict = dict_list[0]
            for d in dict_list[1:]:
                temp_dict.update(d)
        else:
            temp_dict = dict_list[0]
        if group_key:   
            temp_dict['group_key'] = group_key
        if parameter_key:
            temp_dict['param_key'] = parameter_key
        if 'generated_params' in temp_dict:
            temp_dict['generated_params'] = str(temp_dict['generated_params'])
        if isinstance(frame, pandas.DataFrame):
            frame = frame.append(temp_dict, ignore_index=True)
        else:
            keys = temp_dict.keys()
            values = temp_dict.values()
            frame = pandas.DataFrame(columns=keys)
            frame = frame.append(temp_dict, ignore_index = True)
        return frame
                

    def add_computation(self, parameters_dict, result_dict, group_key, parameter_key):
        temp_dict = defaultdict(list)
        self.df = self._add_dict_entries_to_frame(self.df, [parameters_dict, result_dict], group_key, parameter_key)
        if len(self.df) > 100:
            self.flush()

    def insert_computations(self, results_list):
        if isinstance(self.df, pandas.DataFrame):
            try:
                self.df.to_sql(self.fact_table, self.engine, if_exists='append', index=False)
            except:
                time.sleep(5)
            self.df = None

    def add_query(self, parameters_dict, group_key, parameter_key):
        self.params_df = self._add_dict_entries_to_frame(self.params_df, [parameters_dict], parameter_key=parameter_key, group_key=group_key) 
        if len(self.params_df) > 100:
            self.flush()

    def insert_queries(self, query_list):
        if isinstance(self.params_df, pandas.DataFrame):
            try: 
                self.params_df.to_sql(self.params_table, self.engine, if_exists='append', index=False)
            except:
                time.sleep(5)
                self.params_df.to_sql(self.params_table, self.engine, if_exists='append', index=False)
            self.params_df = None

    def post_process(self):
        p = self.params_table
        f = self.fact_table
        query = ('SELECT * INTO ' + self.table_name + ' FROM (SELECT params_table.group_key, params_table.param_key')
        for col in list(self.input_columns) + list(self.params_columns) + list(self.fact_columns):
            if col == 'group_key' or col == 'param_key':
                continue
            else:
                query += ', ' + col
        query += ' FROM ' + p + ' params_table JOIN ' + f + ' fact_table'
        query += ' ON params_table.group_key = fact_table.group_key) A'
        conn = self.engine.connect()
        conn = conn.connection
        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()
        #cursor.execute("DROP TABLE " + f)
        #cursor.execute("DROP TABLE " + p)
        #conn.commit()

        cursor.execute("ALTER TABLE " + self.table_name + " ADD column id SERIAL")
        conn.commit()

    def flush(self):
        if isinstance(self.df, pandas.DataFrame):
            self.insert_computations(None)
        if isinstance(self.params_df, pandas.DataFrame):
            self.insert_queries(None)

    
        

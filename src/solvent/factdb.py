import psycopg2
import json

def insert_fact(conn, fact_type, parameters, result, group_key, parameters_key):
    """
    Insert a fact into the fact table.
    Parameters are result are both dictionaries that are written
    to the table as JSON objects.
    group_key is an optional parameter that can be used to identify
    facts with identical results.
    """
    INSERT = ("INSERT INTO facts (type, parameters, result, group_key, parameters_key) VALUES (%s, %s, %s, %s, %s)");

    params = json.dumps(parameters, default=str)
    r = json.dumps(result, default=str)
    cur = conn.cursor()
    cur.execute(INSERT, [fact_type, params, r, group_key, parameters_key])
    conn.commit()
    cur.close()

def insert_facts(conn, fact_list):
    """ 
    Inserts many facts into the fact table.
    fact_list is (fact_type, parameters, result, group_key)
    parameters and result are converted to json objects
    and are expected to be dictionaries.
    """
    INSERT = ("INSERT INTO facts (type, parameters, result, group_key, parameters_key) VALUES (%s, %s, %s, %s, %s)");
    bundle = []
    for (fact_type, parameters, result, group_key, parameters_key) in fact_list:
        params = json.dumps(parameters, default=str)
        r = json.dumps(result, default=str)
        bundle.append((fact_type, params, r, group_key, parameters_key))
    cur = conn.cursor()
    cur.executemany(INSERT, bundle)
    conn.commit()
    cur.close()

def insert_queries(conn, query_list):
    """ 
    Inserts many query parameters into the parameters table.
    fact_list is (fact_type, query_parameters, group_key)
    parameters are converted to json objects
    and are expected to be dictionaries.
    """
    INSERT = ("INSERT INTO parameters (type, parameters, parameters_key) SELECT %s, %s, %s WHERE NOT EXISTS (SELECT type, parameters, parameters_key FROM parameters WHERE parameters_key = %s)");
    bundle = []
    for (fact_type, parameters, parameters_key) in query_list:
        params = json.dumps(parameters, default=str)
        bundle.append((fact_type, params, parameters_key, parameters_key))
    cur = conn.cursor()
    cur.executemany(INSERT, bundle)
    conn.commit()
    cur.close()

def get_facts_by_type(fact_type):
    SELECT = ("SELECT * FROM facts WHERE type = %s")
    cur = conn.cursor()
    results = cur.execute(SELECT, [fact_type])
    results = results.fetchall()
    cur.close()
    return results 



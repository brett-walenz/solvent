import datetime
import itertools
import redis
import pickle
import bundle

class GlobalCache:
    def __init__(self, redis_host="localhost"):
        self.redis_host = redis_host
        self.cache = redis.StrictRedis(redis_host)

    def get_bundle(self, key, uuid, prefix=False):
        if not prefix:
            key = self._bundle_prefix(uuid) + key
        members = self.cache.smembers(key)
        b = bundle.Bundle(key=key)
        for m in members:
            tup = pickle.loads(m)
            b.add_to_bundle(pickle.loads(m))
        return b 

    def bundle_exists(self, key, uuid):
        return self.cache.exists(self._bundle_prefix(uuid) + key)

    def add_to_bundle(self, key, uuid, parameter):
        self.cache.sadd(self._bundle_prefix(uuid) + key, pickle.dumps(parameter))

    def get_bundles(self, uuid):
        bundles = []
        for key in self.cache.keys(pattern=self._bundle_prefix(uuid) + "*"):
            bundles.append(self.get_bundle(key, uuid, prefix=True))
        return bundles

    def get(self, key, uuid):
        return self.cache.get(self._prefix(uuid) + str(key))

    def set(self, key, uuid, value):
        self.cache.set(self._prefix(uuid) + str(key), value)

    def _prefix(self, uuid):
        return uuid + "_"

    def _bundle_prefix(self, uuid):
        return uuid + "_bundle_"

class LocalCache:
    def __init__(self, cache_columns=None):
        self.cache = dict()
        self.columns = cache_columns

    def set(self, key, value):
        self.cache[key] = value

    def get(self, key):
        return self.cache[key]

    def tolist(self):        
        return self.cache.values() 



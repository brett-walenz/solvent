def add_all(cache_queryable, results, columns):
    values_string = ""
    for c in columns:
        values_string += "?, "
    curs = cache_queryable.conn.connection.cursor()
    curs.executemany("INSERT INTO data VALUES (" + values_string[:-2] + ")", results)
    return cache_queryable 



def no_op(cache_queryable, results, columns):
    return cache_queryable

def sync_dominance_cache(cache_queryable, results, columns, dominance=50):
    values_string = ""
    for c in columns:
        values_string += "?, "
        
    curs = cache_queryable.conn.connection.cursor()
    skyline_query = ("SELECT * FROM data INNER_S "
                     "WHERE dominance_count > ? AND NOT EXISTS ( "
                     "SELECT * FROM data OUTER_S WHERE dominance_count > ? AND "
                     "OUTER_S.p_earned_run_average <= INNER_S.p_earned_run_average AND OUTER_S.p_strikeouts >= INNER_S.p_strikeouts "
                     "AND OUTER_S.p_hits <= INNER_S.p_hits "
                     "AND (OUTER_S.p_earned_run_average < INNER_S.p_earned_run_average OR OUTER_S.p_strikeouts > INNER_S.p_strikeouts "
                     "OR OUTER_S.p_hits < INNER_S.p_hits))")
    curs.executemany("INSERT INTO data VALUES (" + values_string[:-2] + ")", results)
    results = curs.execute("SELECT * FROM data WHERE dominance_count <= ?", (dominance,))
    full_results = results.fetchall()
    boundary_results = curs.execute(skyline_query, (dominance, dominance))
    full_results.extend(boundary_results.fetchall())
    curs.execute("DELETE FROM data")
    curs.executemany("INSERT INTO data VALUES (" + values_string[:-2] + ")", full_results)

    return cache_queryable 



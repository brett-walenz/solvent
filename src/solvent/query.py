import pandas
import sqlite3
import uuid
import os
from pandas.io import sql
from sqlalchemy import create_engine
from sqlalchemy.sql import text
import gc

class RemoteQueryable(object):
    def __init__(self, engine_string):
        self.engine = create_engine(engine_string)
        self.connection = self.engine.connect()

    def sql(self, obj, *multiparams, **params):
        return self.connection.execute(text(obj), *multiparams, **params)

class Queryable(object):
    def __init__(self, values, columns, index=None):
        self.frame = pandas.DataFrame(values, columns=columns)
        #self.id = uuid.uuid4()
        #self.engine = create_engine('sqlite:///' + str(self.id) + ".db")
        self.engine = create_engine('sqlite://')
        self.conn = self.engine.connect()
        self.frame.to_sql('data', self.engine, index=False)
        self.cache_hits = 0
        self.queries = 0
        if index:
            self.conn.execute("CREATE INDEX index2 ON data (" + index + ")")
            #self.conn.execute("CREATE INDEX index_date ON data(person_id, date)")
        self.query_cache = dict() 
        self._index(columns)
        #wipe out the dataframe to free up memory
        self.frame = None
    
    def _lookup(self, query, params):
        key = query + str(params)
        if key in self.query_cache:
            self.cache_hits += 1
            return self.query_cache[key]
        return None

    def _index(self, columns):
        ind_col = ""
        for col in columns:
            ind_col += col + ", "
        self.conn.execute("CREATE INDEX index1 ON data (" + ind_col[:-2] + ")")

    def sql(self, query, *multiparams, **params):
        self.queries += 1
        value = self._lookup(query, params)
        if not value:
            #cursor = self.conn.cursor()
            return self.conn.execute(text(query), *multiparams, **params)
            #if params:
            #    cursor.execute(query, params)
            #else:
            #    cursor.execute(query)
            #value = cursor.fetchall()
        #return value


    def close(self):
        #self.conn.execute("DROP INDEX index2")
        #self.conn.execute("DROP INDEX index_date")
        #self.conn.execute("DROP INDEX index1")
        #self.conn.execute("DROP TABLE data")
        pass
        '''
        try:
            os.remove('/tmp' + str(self.id) + ".db")
        except OSError:
            print("Could not delete database file.") 
        '''

class ComputationCache(Queryable):
    def __init__(self, values=None, columns=None, index=None):
        self.columns = columns  
        self.index = index
        self.query_cache = dict() 
        self.queries = 0
        self.cache_hits = 0
        if not values or len(values) == 0:
            self.frame = None
            self.blank = True
        else: 
            self.init(values)

    def init(self, values):
        if type(values) == tuple:
            values = [values]
        self.frame = pandas.DataFrame(list(values), columns=self.columns)
        self.engine = create_engine('sqlite://')
        self.conn = self.engine.connect()
        self.frame.to_sql('data', self.engine, index=False)
        self.blank = False

        if self.index:
            self.conn.execute("CREATE INDEX index2 ON data (" + index + ")")
        self._index(self.columns)

    def is_blank(self):
        return self.blank

    def local_sync(self, sync_function, results):
        if self.is_blank():
           self.init(results)
        #else:
        sync_function(self, results, self.columns) 
        return self
    
    def global_sync(self, sync_function, unified_cache, results):
        if unified_cache:
            self.init(unified_cache)
        results_list = []
        for key, group in results:
            c = list(group.get_transform())
            if type(group.get_output()) == tuple:
                c.extend(group.get_output())
            else:
                c.append(group.get_output())
            results_list.append(c)
        return self.local_sync(sync_function, results_list)


    def get_contents(self):
        results = []
        if not self.is_blank():
            cursor = self.conn.connection.cursor()
            cursor.execute("SELECT * FROM data")
            results = cursor.fetchall()
        return results 

    def clear(self):
        if not self.is_blank():
            cursor = self.conn.connection.cursor()
            cursor.execute("DELETE FROM data")


    def size(self):
        size = 0
        if not self.is_blank():
            cursor = self.conn.connection.cursor()
            cursor.execute("SELECT count(1) from data")
            results = cursor.fetchone()
            size = results[0]

        return size

    def parallelize(self, context):
        if self.is_blank():
            cache_data = None
        else:
            cache_data = self.get_contents()

        #print("parallelizing: ", len(cache_data))
        cache_rdd = context.parallelize([cache_data], 1)
        cache_rdd.glom() 
        return cache_rdd

GEQ = ">="
LEQ = "<="
INEQ_MAP = {">=":">", "<=":"<"}

def create_dominance_count_query(columns, comparators, values):
    q = "SELECT count(1) FROM data OUTER_S WHERE "
    part = "" 
    for column, comp, value in zip(columns, comparators, values):
        if part:
            part += " AND "
        part += "OUTER_S." + column + " " + comp + " " + str(value)

    part += " AND ("
    or_part = "" 
    global INEQ_MAP
    for column, comp, value in zip(columns, comparators, values):
        if or_part:
            or_part += " OR " 
        or_part += "OUTER_S." + column + " " + INEQ_MAP[comp] + " " + str(value)
    part += or_part
    part += ")"
    return q + part


'''
def create_dominance_count_query(inner_query, columns, comparators):
    q = "SELECT count(1) FROM data OUTER_S, (" + inner_query + ") INNER_S WHERE " 
    part = None
    for column, comp in zip(columns, comparators):
        if part:
            part += " AND " 
        part += "OUTER_S." column + " " +  comp +  " INNER_S." + column

    part += " AND ("    
    for column, comp in zip(columns, comparators):
        part += "OUTER_S." + column + " " + INEQ_MAP[comp] + " INNER_S." + column
    part += ")"

    return q + part
'''
